let express = require('express')
let app = express()
let port = 3000 
let server = app.listen(port)

app.use(express.static('public'))

console.log("My server is running on port: " + port)

let socket = require('socket.io')
let io = socket(server)
io.sockets.on('connection',newConnection);

function newConnection(socket){
    console.log("new connection")
    console.log(socket.id)   
    socket.on('lineData',saveTheData)
    socket.on('saveDatabase',saveTheDatabase)
}

fs = require('fs')

function saveTheData(inData){
    console.log(inData)
    let theFolder = "public/drawings/" + inData.image + "/"

    if (!fs.existsSync(theFolder)){
        fs.mkdirSync(theFolder);
    }
    console.log("THE FOLDER" + theFolder)
    // let existingJSONFile = loadJSON(theFolder+"lines.json");

    let rawData = JSON.stringify(inData)
    fs.writeFileSync( theFolder + 'lines.json',rawData)
     
}

function saveTheDatabase(database){
    let theFolder = "public/doornroosje/"
    let rawData = JSON.stringify(database)
    fs.writeFileSync(theFolder + 'database.json',rawData)
}
