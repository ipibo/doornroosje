JSONObject json;



void setup() {
  size(600, 600);
  json = loadJSONObject("../drawings/8640/lines.json");

  JSONArray values = json.getJSONArray("lines");

  //colorMode(HSB, 100);


  //println(values.size());

  for (int j = 0; j < values.size(); j++) {

    JSONArray theX = values.getJSONObject(j).getJSONArray("x");
    JSONArray theY = values.getJSONObject(j).getJSONArray("y");
    JSONArray theColor = values.getJSONObject(j).getJSONArray("color");
    //println(theColor);


    //stroke(255, 255, 255);
    if (theColor != null) {
      color c = color(theColor.getFloat(0), theColor.getFloat(1), theColor.getFloat(2));
      println(theColor.getFloat(0), theColor.getFloat(1), theColor.getFloat(2));
      //println("hi");
      //color c = HSL2RGB(theColor.getFloat(0), theColor.getFloat(1), theColor.getFloat(2));
      //println(red(c), green(c), blue(c));

      //toRGB(theColor.getFloat(0), theColor.getFloat(1), theColor.getFloat(2), 1);
      //c = color(64, 191, 162);
      stroke(c);
    }

    for (int i = 0; i < theX.size()-1; i++) {
      int x = theX.getInt(i);
      int y = theY.getInt(i);
      int nextX = theX.getInt(i+1);
      int nextY = theY.getInt(i+1);
      //println(x, y);
      line(x, y, nextX, nextY);
    }
  }






  //for(int i = 0; i < values.length; i ++){


  //}
}


void draw() {
}



void toRGB(float h, float s, float l, float alpha) {



  if (s < 0.0f || s > 100.0f) {
    String message = "Color parameter outside of expected range - Saturation";
    throw new IllegalArgumentException(message);
  }

  if (l < 0.0f || l > 100.0f) {
    String message = "Color parameter outside of expected range - Luminance";
    throw new IllegalArgumentException(message);
  }

  if (alpha < 0.0f || alpha > 1.0f) {
    String message = "Color parameter outside of expected range - Alpha";
    throw new IllegalArgumentException(message);
  }

  // Formula needs all values between 0 - 1.

  h = h % 360.0f;
  h /= 360f;
  s /= 100f;
  l /= 100f;

  float q = 0;

  if (l < 0.5)
    q = l * (1 + s);
  else
    q = (l + s) - (s * l);

  float p = 2 * l - q;

  float r = Math.max(0, HueToRGB(p, q, h + (1.0f / 3.0f)));
  float g = Math.max(0, HueToRGB(p, q, h));
  float b = Math.max(0, HueToRGB(p, q, h - (1.0f / 3.0f)));

  r = Math.min(r, 1.0f);
  g = Math.min(g, 1.0f);
  b = Math.min(b, 1.0f);

  //return new Color(r, g, b, alpha);
  //r = r * 255;
  //g = g * 255;
  //b = b * 255;

  println(r, g, b, alpha);
}
