let json

var uitlegshow = 1;

function preload(){
json = loadJSON("doornroosje/database.json")
}
let yearInUrl
let selectedYear
let search
let whatToLookFor = ""
function setup(){

noCanvas()
let foto = json.foto
let url = window.location.href
var regex = /year=\d+/g;
var lookForMatch = url.match(regex);

if(lookForMatch != null){
    let idRegex = /\d+/g
    let theId = lookForMatch[0].match(idRegex)
    yearInUrl = theId[0]
    selectedYear = yearInUrl
    whatToLookFor = "year"
}

regex = /search=.+/g;
lookForMatch = url.match(regex);
if(lookForMatch != null){
  let idRegex = /\w+/g
  search = lookForMatch[0].match(idRegex)
  search = search[1]
  whatToLookFor = "search"
}else{
  search = ""
}
let foundSomething = false;

jaren()

foto.forEach(e => {

  let jaar = e.jaar
  let date = e.date.replaceAll("-","")  
  let year = date.substring(0,4)

  let month = date.substring(4,6)
  let day = date.substring(6,8)
  
  if(whatToLookFor == "year"){
   document.getElementById("uitleg").style.display = "none";
    if(year == selectedYear || selectedYear == null){        

      let fileName = e.fileName
      let id = e.id
    
      src = "small/"+ jaar+"/"+fileName
      src = src.replaceAll(" ","%20") // zorgt er voor dat de spaties weggehaald worden. 
      src = src.replaceAll('[)]',"%29") // zorgt er voor dat de ) weggehaald worden. 
      src = src.replaceAll('[()]',"%28") // zorgt er voor dat de ) weggehaald worden. 
    
      let x = document.createElement("div")
      x.setAttribute("class","photo")
      x.setAttribute("style", 
      "background-image: url("+src+");"
      );

      //to append the text
      let text = document.createElement("div")
      text.setAttribute("class","phototext")
      text.innerHTML = e.onderwerp + " "+ day +"/" + month
      text.setAttribute("style",
        "    position: relative; top: 220px; text-align: center; color: grey; font-size: 14px;"
      );
      x.appendChild(text)
      
      let link = document.createElement("a")
      link.setAttribute('href', '/editPhoto.html?id='+id);
      
      link.appendChild(x)
      document.body.appendChild(link) 
    }
 }
 
 else if (whatToLookFor == "search"){
    let regexSearch = search
    
    regexSearch = regexSearch.toLowerCase();
    let onderwerpString = e.onderwerp.toLowerCase();

    let searchMatch = onderwerpString.match(regexSearch);

    if(searchMatch!=null){
      console.log(searchMatch)
      let fileName = e.fileName
      let id = e.id
    
      src = "small/"+ jaar+"/"+fileName
      src = src.replaceAll(" ","%20") // zorgt er voor dat de spaties weggehaald worden. 
      src = src.replaceAll('[)]',"%29") // zorgt er voor dat de ) weggehaald worden. 
      src = src.replaceAll('[()]',"%28") // zorgt er voor dat de ) weggehaald worden. 
    
      let x = document.createElement("div");
      x.setAttribute("class","photo")
      x.setAttribute("style", 
      "background-image: url("+src+");"
      );
      
      //to append the text
      let text = document.createElement("div")
      text.setAttribute("class","phototext")
      text.innerHTML = e.onderwerp
      text.setAttribute("style",
        "    position: relative; top: 220px; text-align: center; color: grey; font-size: 14px;"
      );
      x.appendChild(text)

      let link = document.createElement("a")
      link.setAttribute('href', '/editPhoto.html?id='+id);

      document.body.appendChild(link)
      link.appendChild(x)
      foundSomething = true
      console.log("TRUE")
    }
 }
});

if(whatToLookFor == "search" && !foundSomething){
  let nothingFoundDiv = document.createElement("p")
  nothingFoundDiv.innerHTML = "Er kon niks gevonden worden voor de zoekterm: " + search + "<br><br>Er kunnen verschillende dingen aan de hand zijn: <br>- Er zijn geen foto's bij van dit evenement<br>- De naam van dit de artiest is anders gespeld in het archief <br>- Er gaat even iets helemaal mis met het systeem hier<br><br>Je kan proberen om: <br>- Nog eens te zoeken met een andere zoekterm <br>- Door de foto's van een jaar bladeren"
  document.body.appendChild(nothingFoundDiv)
}
}

document.ontouchmove = function(event){
event.preventDefault();
}


String.prototype.replaceAll = function(search, replacement) {
var target = this;
return target.replace(new RegExp(search, 'gi'), replacement);
};


function jaren(){
let buttonMenu = document.getElementById("buttonMenu")
buttonMenu.innerHTML = ""

let element = document.createElement("a")
// element.setAttribute("href","?year=")
// element.setAttribute("class","optionButton")
// element.innerHTML = "alle"  
buttonMenu.appendChild(element)

if(yearInUrl!=null){
  console.log("HEEUEJ")
}

for(let i = 2018; i > 1979; i--){
    let element = document.createElement("a")
    element.setAttribute("href","?year="+i)
    element.setAttribute("class","optionButton")
    if(i == selectedYear){
      element.setAttribute("class","selectedYear")
    }
    element.innerHTML = i
    buttonMenu.appendChild(element)
  }//for
}//jaren

function ruimtes(){
let buttonMenu = document.getElementById("buttonMenu")
buttonMenu.innerHTML = ""
let deRuimtes = ['Kelder', 'Fietsenhok']

deRuimtes.forEach(ruimte => {
  let element = document.createElement("a")
  element.setAttribute("href","?ruimte="+ruimte)
  element.setAttribute("class","optionButton")
  element.innerHTML = ruimte
  buttonMenu.appendChild(element)
});
}//ruimtes

function keyPressed(){
if(keyCode == 13){
  let zoekbalk = document.getElementById("zoekbalk")
  if(zoekbalk.value != "Zoekbalk"){
    window.location.href = "?search=" +zoekbalk.value; 
  }
}
}//keypressed


function goTo(id) {
  $('html, body').animate({scrollTop:$(id).position().top}, 'slow');
}//goto 

function uitlegweg(){
 document.getElementById("uitleg").style.opacity = "0";
 document.getElementById("uitleg").style.pointerEvents = "none";
}

function uitleglader(){

  setTimeout(function(){ document.getElementById("uitleg").style.opacity = "1"; }, 1000);

}