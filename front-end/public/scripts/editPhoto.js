let socket
let buttonKlaar, buttonOpnieuw, buttonTerug
let img
let existingJSON
let JSONAvailible = false;
let dataBaseJson
let imgId 
let the_color_for_this_person

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
let id
function preload(){
    let url = window.location.href
    var regex = /id=\d+/g
    var lookForMatch = url.match(regex)
    
    if(lookForMatch != null){
        let idRegex = /\d+/g
        let theId = lookForMatch[0].match(idRegex)
        id = theId[0]
    }
    imgId = id
    if(UrlExists("drawings/"+id+"/lines.json")){        
        existingJSON = loadJSON("drawings/"+id+"/lines.json")
        JSONAvailible = true
    }else{
        // console.log("hi")
        existingJSON = {
            "image":id,
            "lines":[]
    }
        JSONAvailible = false
    }
    dataBaseJson = loadJSON("doornroosje/database.json")
}

let jaar
let date
let fileName
let fotoId
let oneLine

function setup(){
    var myCanvas = createCanvas(700,700);
    myCanvas.parent("wrapper");

    disableScroll()
    //socket = io.connect('http://192.168.0.6:3000')    //dit is belangrijk dat dit het i.p. adress is van de server
    socket = io.connect('http://10.130.6.217:3000')    //dit is belangrijk dat dit het i.p. adress is van de server
  
    // De goede foto zoeken via de href
    let url = window.location.href
    var regex = /id=\d+/g;
    var lookForMatch = url.match(regex);
    let id
    if(lookForMatch != null){
        let idRegex = /\d+/g
        let theId = lookForMatch[0].match(idRegex)
        id = theId[0]
    }
    let src = ""
    dataBaseJson.foto.forEach(foto => {
        if(foto.id == id){
            jaar = foto.jaar
            date = foto.date.replaceAll("-","")  
            fileName = foto.fileName
            fotoId = foto.id
            src = "doornroosje/"+ jaar+"/"+fileName
        }
    });

    img = loadImage(src)

    // het toevoegen van de buttons op de pagina
    buttonTerug = createButton('Terug')
    buttonTerug.class("buttonTerug")
    buttonTerug.mousePressed(goBackToIndex)
    
    buttonOpnieuw = createButton("Opnieuw")
    buttonOpnieuw.class("buttonOpnieuw")
    buttonOpnieuw.mousePressed(clearTheImage)

    buttonKlaar = createButton('Klaar')
    buttonKlaar.class("buttonKlaar")
    buttonKlaar.mousePressed(saveTheImage)
    buttonKlaar.mouseReleased(goBackToStart)
    
    buttonTerug.parent("wrapper")
    buttonOpnieuw.parent("wrapper")
    buttonKlaar.parent("wrapper")

    the_color_for_this_person = [random(255),50,50]

    oneLine = {
        x : [],
        y : [],
        counter : 0,
        color: the_color_for_this_person
    }}

function draw(){    
    background(0)
    imageMode(CENTER);

    //dit zorgt er voor dat de afbeeldingen de goede verhouding hebben
    if(img.width > img.height){
        let mappedHeight = map(img.height,0,img.width,0,height)
        image(img, width/2,height/2,width,mappedHeight);
    }else if(img.width < img.height){
        let mappedWidth = map(img.width,0,img.height,0,width)
        image(img, width/2,height/2,mappedWidth,height);
    }else{
        image(img,width/2,height/2,width,height)
    }

    if(JSONAvailible){
        showTheLines(existingJSON)
    }
    colorMode(HSL)
    //dit is de huidige lijn
    for (let i = 0; i < oneLine.x.length-1; i++) {
        stroke(the_color_for_this_person)

        strokeWeight(5)
        line(oneLine.x[i],oneLine.y[i],oneLine.x[i+1],oneLine.y[i+1])
    }
    showTheLines(allLines)
}

function showTheLines(allTheLines){
    // // dit zijn alle lijnen
    // console.log(allTheLines)
    allTheLines.lines.forEach(thisLine => {
        stroke(255)
        if(thisLine.color != null){
            stroke(thisLine.color)
        }

        strokeWeight(5)
        for(let i = 0 ; i < thisLine.x.length-1; i++){
            line(thisLine.x[i],thisLine.y[i],thisLine.x[i+1],thisLine.y[i+1])
        }
    })
}

function keyPressed(){
    if(keyCode == 83){
        console.log("SAVE")
        saveTheImage()
    }
}

function saveTheImage(){
    allLines.lines.forEach(line => {
        console.log(line)
        console.log(existingJSON)
        existingJSON.lines.push(line)
    });

    socket.emit('lineData',existingJSON)

    console.log("the database")

    dataBaseJson.foto.forEach(foto => {
        if(foto.id == id){
            console.log(foto)
            foto.laatsaangepast = Date.now()
        }
    });
    socket.emit('saveDatabase',dataBaseJson)

}


function clearTheImage(){
    allLines = {
        image : "thisImage", // I will have to change this in the future
        lines : [],
        imageId : 0,
    }
    saveTheImage(existingJSON)
}

let allLines = {
    image : "thisImage", // I will have to change this in the future
    lines : [],
    imageId : 0,
}

    
function mouseDragged(){
    console.log("dragged")
    oneLine.x.push(mouseX)
    oneLine.y.push(mouseY)
    oneLine.counter++
}

function mouseReleased(){
    allLines.lines.push(oneLine)
    console.log("mouseRelease")
    oneLine = {
        x : [],
        y : [],
        counter : 0,
        color: the_color_for_this_person
    }
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
  };
  
function UrlExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

function goBackToIndex(){
    // window.location.href = "index.html";
    window.history.back();
}

function goBackToStart(){
    window.location.href = "index.html";
}


function uitlegweg(){
   document.getElementById("uitleg").style.opacity = "0";
   document.getElementById("uitleg").style.pointerEvents = "none";
}
