let dataBaseJson
let existingJSON
let JSONAvailible

let timer = 5

let lastImage
let anImage
let allImages = []

//let imageSize = 1200
let imageSize = 600
let mappedWidthGlobal = 0 

let xLocation
let yLocation

let xLocations = []
let yLocations = []

let allLoadedId = []
let theDatabase

let linesLoadedBool = false
let theLines
let allLines =[]

let databaseLoaded = false

//-------------------------------------------------------------------------------------------------------------------------------------------

function setup(){
    //createCanvas(3840,1280)
    createCanvas(1000,640)
    loadJSON("doornroosje/database.json",dataLoaded)
}

function draw(){
    background(0)
    timerFunction()

    if(databaseLoaded){
        if(allImages.length != 0 ){            
            for(let i = 0; i < allImages.length ; i++){
                displayTheImage(allImages[i],xLocations[i],yLocations[i],imageSize)
                if(allLines[i].info != 'nolines'){
                    showTheLines(allLines[i],xLocations[i],yLocations[i],imageSize)
                }
                xLocations[i]+=5
                if(xLocations[i] >0 && xLocations[i] < 10 && startingToLoad == false){
                    loadNextImage()
                }
            }
        }else if(allImages.length == 0  && startingToLoad == false){
            loadNextImage()
        }
    }
}

let startingToLoad = false

function loadNextImage(){
    startingToLoad = true
    let imgId = 0
    if(chooseNextImage){
        imgId = nextImageId
        chooseNextImage = false
    }else{
        let randomPicker = random(1)
        if(randomPicker<.5){
            imgId = chooseOneFromTheList()
        }else{

        imgId = random(100,8000)
        imgId = parseInt(imgId)
        }
    }
    loadImage(getNewImage(theDatabase,imgId),imageLoaded)
    if(UrlExists("drawings/"+imgId+"/lines.json")){
        loadJSON("drawings/"+imgId+"/lines.json",linesLoaded)
    }
    else{
        theLines ={
            'info': 'nolines'
        }
        linesLoadedBool=false
        allLines.push(theLines)
    }
}

function chooseOneFromTheList(){
    let imagesWithDrawing = []
    theDatabase.foto.forEach(element => {
        if(element.laatsaangepast != null){
            imagesWithDrawing.push(element.id)
        }
    });

    let lengthOfTheList = imagesWithDrawing.length
    console.log(lengthOfTheList)
    let pickRandom = random(lengthOfTheList)
    pickRandom = parseInt(pickRandom)
    let onewithdrawing = imagesWithDrawing[pickRandom]

    return onewithdrawing

    // console.log(imagesWithDrawing)

    // return imagesWithDrawing
}

//-------------------------------------------------------------------------------------------------------------------------------------------

let lastImageTime = 0
let lastImageId = 0 
let chooseNextImage = false
let nextImageId = 0

function dataLoaded(database){
    console.log("database reloaded")
    theDatabase = database
    databaseLoaded = true
   if (lastImageId != (getLastedEditedImage(theDatabase))){
        lastImageId = getLastedEditedImage(theDatabase)
        chooseNextImage = true
        nextImageId = lastImageId
   }
}

function getLastedEditedImage(database){
    let id = 0
    let time = 0
    let oi
    
    database.foto.forEach(element => {  
        if(element.laatsaangepast != null){
            if(element.laatsaangepast > lastImageTime){
                lastImageTime = element.laatsaangepast
                id = element.id
                time = lastImageTime
                lastImageId = id
            }else{
                id = lastImageId
            }
        }
    });

    return id
}

let mappedWidths = []
let mappedHeights = []

function imageLoaded(theImage){
    console.log("the Image Is loded")
    anImage = theImage

    let size = imageSize
    let mappedHeight
    let mappedWidth

    if(theImage.width > theImage.height){
        mappedHeight = map(theImage.height,0,theImage.width,0,size)
        thisXLocation = 0-size

        mappedWidths.push(size)
        mappedHeights.push(mappedHeight)
    }else if(theImage.width < theImage.height){
        mappedWidth = map(theImage.width,0,theImage.height,0,size) 
        thisXLocation = 0-size

        mappedWidths.push(mappedWidth)
        mappedHeights.push(size)
        
    }else{
        mappedWidths.push(size)
        mappedHeights.push(size)
        thisXLocation = 0-size
    }

    
    xLocations.push(thisXLocation)
    yLocations.push(50)

    allImages.push(theImage)
    startingToLoad = false
}


function linesLoaded(lines){
    console.log("the Lines are loaded")
    theLines = lines
    linesLoadedBool = true
    allLines.push(lines)
}

//-------------------------------------------------------------------------------------------------------------------------------------------

function getNewImage(database,id){
    database.foto.forEach(foto => {
        if(foto.id == id){
            jaar = foto.jaar
            date = foto.date.replaceAll("-","")  
            fileName = foto.fileName
            fotoId = foto.id
            src = "doornroosje/"+ jaar+"/"+fileName
        }
    });
    return src
}

//-------------------------------------------------------------------------------------------------------------------------------------------


function displayTheImage(theImage,x,y,size){
    //dit zorgt er voor dat de afbeeldingen de goede verhouding hebben
    imageMode(CENTER)
    rectMode(CENTER)
    if(theImage.width > theImage.height){
        let mappedHeight = map(theImage.height,0,theImage.width,0,size)
        x = x + size/2
        y = y + size/2
        
        
        // rect(x,y,size,size)
        ellipse(x,y,10,10)
        image(theImage, x,y,size,mappedHeight)

    }else if(theImage.width < theImage.height){
        let mappedWidth = map(theImage.width,0,theImage.height,0,size)
        mappedWidthGlobal = mappedWidth
        x = x + size/2
        y = y + size/2
                
        // rect(x,y,size,size)
        ellipse(x,y,10,10)
        image(theImage, x,y,mappedWidth,size)
    }else{

        // rect(x,y,size,size)
        ellipse(x,y,10,10)
        image(theImage,x,y,size,size)
    }
    
}

// laat de lijnen op de goede plek en grootte zien
function showTheLines(allTheLines,x,y,size){
    allTheLines.lines.forEach(thisLine => {
        stroke(255)
        colorMode(HSL)
        if(thisLine.color != null){
            stroke(thisLine.color)
        }
        strokeWeight(8)
        for(let i = 0 ; i < thisLine.x.length-1; i++){
            let x1 = map(thisLine.x[i],0,700,0,size)
            let y1 = map(thisLine.y[i],0,700,0,size)
            let x2 = map(thisLine.x[i+1],0,700,0,size)
            let y2 = map(thisLine.y[i+1],0,700,0,size)
            line(x1+x,y1+y,x2+x,y2+y)
        }
    })
}

//-------------------------------------------------------------------------------------------------------------------------------------------

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function UrlExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}


function reloadDataBase(){
    loadJSON("doornroosje/database.json",dataLoaded)
}

function timerFunction(){

    if (frameCount % 60 == 0 && timer > 0) { // if the frameCount is divisible by 60, then a second has passed. it will stop at 0
        timer --;
    }
    if (timer == 0) {
        reloadDataBase()
        timer = 5
    }

}
